(use abnf abnf-charlist)

(define <wsp> (drop-consumed (alternatives (:c (integer->char #x20))
                                           (:c (integer->char #x9))
                                           (:c (integer->char #xD))
                                           (:c (integer->char #xA)))))

(define <digit> (alternatives (:c #\1) (:c #\2) (:c #\3) (:c #\4)
                              (:c #\5) (:c #\6) (:c #\7) (:c #\8)
                              (:c #\9) (:c #\0)))

(define <digit-sequence> (repetition1 <digit>))

(define <sign> (alternatives (:c #\+) (:c #\-)))

(define <exponent> (concatenation
                    (alternatives (char #\e) (char #\E))
                    (optional-sequence <sign>)
                    <digit-sequence>))

(define <fractional-constant>
  (alternatives (::
                 (:? <digit-sequence>)
                 (:c #\.)
                 <digit-sequence>)
                (::
                 <digit-sequence>
                 (:c #\.))))

(define <floating-point-constant>
  (alternatives
   (:: <fractional-constant>
       (:? <exponent>))
   (::
    <digit-sequence>
    <exponent>)))

(define <integer-constant> <digit-sequence>)

(define <comma> (drop-consumed (:c #\,)))

(define <comma-wsp>
  (drop-consumed (alternatives
                  (:: (:+ <wsp>)
                      (:? <comma>)
                      (:* <wsp>))
                  (:: <comma> (:* <wsp>)))))

(define <flag> (alternatives (:c #\0) (:c #\1)))

(define (number n)
  (let ((n (list->string (reverse n))))
    (list (string->number n))))

(define <number>
  (bind
   number
   (alternatives
    (:: (:? <sign>)
        <floating-point-constant>)
    (:: (:? <sign>)
        <integer-constant>))))

(define <nonnegative-number>
  (alternatives
   <integer-constant>
   <floating-point-constant>))

(define <coordinate> <number>)

(define <coordinate-pair>
  (bind
   reverse
   (:: <coordinate> (drop-consumed (:? <comma-wsp>)) <coordinate>)))

(define <elliptical-arc-argument>
  (::
   <nonnegative-number>
   (:* <comma-wsp>)
   <nonnegative-number>
   (:* <comma-wsp>)
   <number>
   <comma-wsp>
   <flag>
   (:* <comma-wsp>)
   <flag>
   (:* <comma-wsp>)
   <coordinate-pair>))

(define <elliptical-arc-argument-sequence>
  (::
   <elliptical-arc-argument>
   (:* (::
        (:? <comma-wsp>)
        <elliptical-arc-argument>))))

(define <elliptical-arc>
  (bind
   (lambda (a) `((elliptical-arc ,@a)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\A))
                     (bind (lambda _ '(relative)) (:c #\a)))
       (:* <wsp>)
       <elliptical-arc-argument-sequence>)))

(define <smooth-quadratic-bezier-curveto-argument-sequence>
  (::
   <coordinate-pair>
   (:* (:: (:? <comma-wsp>)
           <coordinate-pair>))))

(define <smooth-quadratic-bezier-curveto>
  (bind
   (lambda (s) `((smooth-bezier ,@s)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\T))
                     (bind (lambda _ '(relative)) (:c #\t)))
       (:* <wsp>)
       <smooth-quadratic-bezier-curveto-argument-sequence>)))

(define <quadratic-bezier-curveto-argument>
  (:: <coordinate-pair> (:? <comma-wsp>) <coordinate-pair>))

(define <quadratic-bezier-curveto-argument-sequence>
  (:: <quadratic-bezier-curveto-argument>
      (:* (:: (:? <comma-wsp>)
              <quadratic-bezier-curveto-argument>))))

(define <quadratic-bezier-curveto>
  (bind
   (lambda (b) `((quadratic-bezier ,@b)))
   (:: (drop-consumed (alternatives (:c #\Q) (:c #\q)))
       (:* <wsp>)
       <quadratic-bezier-curveto-argument-sequence>)))

(define <smooth-curveto-argument>
  (:: <coordinate-pair> (:? <comma-wsp>) <coordinate-pair>))

(define <smooth-curveto-argument-sequence>
  (:: <smooth-curveto-argument>
      (:* (:: (:? <comma-wsp>)
              <smooth-curveto-argument>))))

(define <smooth-curveto>
  (bind
   (lambda (s)
     `((smooth-curve ,@s)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\S))
                     (bind (lambda _ '(relative)) (:c #\s)))
       (:* <wsp>)
       <smooth-curveto-argument-sequence>)))

(define <curveto-argument>
  (:: <coordinate-pair>
      (:? <comma-wsp>)
      <coordinate-pair>
      (:? <comma-wsp>)
      <coordinate-pair>))

(define <curveto-argument-sequence>
  (:: <curveto-argument>
      (:* (:: (:? <comma-wsp>)
              <curveto-argument>))))

(define <curveto>
  (bind
   (lambda (c) `((curve-to ,@c)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\C))
                     (bind (lambda _ '(relative)) (:c #\c)))
       (:* <wsp>)
       <curveto-argument-sequence>)))

(define <vertical-lineto-argument-sequence>
  (:: <coordinate>
      (:* (:: (:? <comma-wsp>)
              <coordinate>))))

(define <vertical-lineto>
  (bind
   (lambda (t)
     `((vertical-lineto ,@t)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\V))
                     (bind (lambda _ '(relative)) (:c #\v)))
       (:* <wsp>)
       <vertical-lineto-argument-sequence>)))

(define <horizontal-lineto-argument-sequence>
  (:: <coordinate>
      (:* (:: (:? <comma-wsp>)
              <coordinate>))))

(define <horizontal-lineto>
  (bind
   (lambda (t)
     `((horizontal-lineto ,@t)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\H))
                     (bind (lambda _ '(relative)) (:c #\h)))
       (:* <wsp>)
       <horizontal-lineto-argument-sequence>)))

(define <lineto-argument-sequence>
  (:: <coordinate-pair>
      (:* (:: (:? <comma-wsp>)
              <coordinate-pair>))))

(define <lineto>
  (bind
   (lambda (l) `((line-to ,@l)))
   (:: (alternatives (bind (lambda _ '(absolute)) (:c #\L))
                     (bind (lambda _ '(relative)) (:c #\l)))
       (:* <wsp>)
       <lineto-argument-sequence>)))

(define <closepath> (bind (lambda _ '((close-path)))
                          (drop-consumed (alternatives (:c #\Z) (:c #\z)))))

(define <moveto-argument-sequence>
  (alternatives
   <coordinate-pair>
   (:: <coordinate-pair> (:? <comma-wsp>)
       <lineto-argument-sequence>)))



(define <moveto>
  (bind (lambda (m) `((move-to ,@m)))
        (::  (alternatives (bind (lambda _ '(absolute)) (:c #\M))
                           (bind (lambda _ '(relative)) (:c #\m)))
            (:* <wsp>)
            <moveto-argument-sequence>)))

(define <drawto-command>
  (alternatives
   <closepath>
   <lineto>
   <horizontal-lineto>
   <vertical-lineto>
   <curveto>
   <smooth-curveto>
   <quadratic-bezier-curveto>
   <smooth-quadratic-bezier-curveto>
   <elliptical-arc>))

(define <drawto-commands>
  (:: <drawto-command> (:* (:: (:* <wsp>) <drawto-command>))))

(define <moveto-drawto-command-group>
  (:: <moveto> (:* <wsp>) (:? <drawto-commands>)))

(define <moveto-drawto-command-groups>
  (:: <moveto-drawto-command-group> (:* (:: (:* <wsp>) <moveto-drawto-command-group>))))

(define <svg-path>
  (bind reverse
        (:: (:* <wsp>) (:? <moveto-drawto-command-groups>) (:* <wsp>))))

(bind <coordinate> number)
;; (bind <moveto> moveto)
;; (bind <closepath> closepath)
;; (bind <lineto> lineto)
;; (bind <horizontal-lineto> horizontal-lineto)
;; (bind <vertical-lineto> vertical-lineto)
;; (bind <curveto> curveto)
;; (bind <smooth-curveto> smooth-curveto)
;; (bind <quadratic-bezier-curveto> quadratic-bezier-curveto)
;; (bind <smooth-quadratic-bezier-curveto> smooth-quadratic-bezier-curveto)
;; (bind <elliptical-arc> elliptical-arc)


(define (->char-list s)
  (if (string? s) (string->list s) s))

(define (parse-error e)
  (print "parse error in svg path: " e)
  `(error))

(define (parser p)
  (<svg-path> (lambda (r) (and (pair? r) (car r))) error `(() ,(->char-list p))))

(define test "M781.68,324.4l-2.31,8.68l-12.53,4.23l-3.75-4.4l-1.82,0.5l3.4,13.12l5.09,0.57l6.79,2.57v2.57l3.11-0.57l4.53-6.27v-5.13l2.55-5.13l2.83,0.57l-3.4-7.13l-0.52-4.59L781.68,324.4L781.68,324.4z")
(define t2 "M406.89,298.34l-0.13,1.11l6.92-0.1l0.35-1.03l-0.15-1.04l-1.99,0.81L406.89,298.34L406.89,298.34z")
(define t "M 1.2,3.4")
(parser t)

